import random

#Creating a blank list
lista = []

#Add a random number to list
def addRandomToList(list):
    list.append(random.randint(0,999))

print('Lista antes:')
print(lista)

addRandomToList(lista)
print('Lista post funcion')
print(lista)

# What if integer is passed to addRandomToList?
# myNum = 0
# addRandomToList(myNum)
# print(myNum)
# Throws AttributeError: 'int' object has no attribute 'append'

#Function with try-catch
def addRandImproved(list):
    try:
        list.append(random.randint(0,999))
    except:
        print('Error: No es una lista.')

# Lets try again:
myNum = 0
addRandImproved(myNum)
print(myNum)

# -----
# Iteration on Lists

def addNRand(list,n):
    try:
        for i in range(0,n):
            list.append(random.randint(0,999))
    except:
        print('Error: No es una lista.')

print('Agrego 10 numeros random a la lista. Lista:')
print(lista)
addNRand(lista,10)
print('Lista despues:')
print(lista)

