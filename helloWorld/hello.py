#This is a comment 
print('Hello world!')

#This is a function 
def hello():
    print('Hello! from function')
hello()

#This is a function with parameter
def helloParam(name):
    print('Hello '+name+'! from function with param')
helloParam('Iqual')

#This is an assignament
myName = 'Juani'
helloParam(myName)

#Return value from function
def sayHello(to):
    return 'Hello '+to+'! from returning value'
print(sayHello('Iqual networks'))